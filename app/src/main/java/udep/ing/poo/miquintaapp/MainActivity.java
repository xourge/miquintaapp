package udep.ing.poo.miquintaapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Collections;
import java.util.Set;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        botonIniciarSesion = (Button)findViewById(R.id.button);
        botonRegistrar = (Button)findViewById(R.id.button2);
        editUsuario = (EditText)findViewById(R.id.editTextUsuario);
        editContrasena = (EditText)findViewById(R.id.editTextContrasena);

        botonIniciarSesion.setOnClickListener(this);
        botonRegistrar.setOnClickListener(this);

        Bundle bundle = getIntent().getExtras();
        if (bundle!=null) {
            String usuario = (String) bundle.get("usuario");
            editUsuario.setText(usuario);
        }
    }

    Button botonIniciarSesion, botonRegistrar;
    EditText editUsuario, editContrasena;

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.button) {

            String inputUsuario = editUsuario.getText().toString();
            String inputContrasena = editContrasena.getText().toString();

            if (inputUsuario.equals("")) { // usuario está en blanco
                Toast.makeText(this,getString(R.string.alertaUsuario),Toast.LENGTH_SHORT).show();

            } else if (inputContrasena.equals("")) { // contraseña está en blanco
                Toast.makeText(this,getString(R.string.alertaContrasena),Toast.LENGTH_SHORT).show();

            } else {

                // leer los datos de la memoria de la app

                Usuarios usuarios = new Usuarios(this.getSharedPreferences("miquintaapp",Context.MODE_PRIVATE));

                if (usuarios.validar(inputUsuario,inputContrasena)) {
                    Intent intent = new Intent(this,HomeActivity.class);
                    intent.putExtra("usuario",inputUsuario);
                    startActivity(intent);
                } else {

                    Toast.makeText(this,getString(R.string.alertaUsuarioContrasena),Toast.LENGTH_SHORT).show();

                    // notificar al usuario que se puede registrar

                    NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "miquintaapp");
                    builder.setContentTitle("¿No estás registrado?"); // título
                    builder.setContentText("Presiona aquí para registrarte"); // descripcion
                    builder.setSmallIcon(R.drawable.ic_launcher_foreground); // icono
                    builder.setAutoCancel(true); // se cierra al lanzar el Intent

                    Intent intent = new Intent(this, SignupActivity.class);
                    PendingIntent pIntent = PendingIntent.getActivity(this,0,intent,0);

                    builder.setContentIntent(pIntent); // qué activity abrir desde la notificación

                    NotificationManager manager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
                    manager.notify(1,builder.build()); // muestro la notificación
                }
            }

        }

        else if (v.getId() == R.id.button2) {

            Intent intent = new Intent(this, SignupActivity.class);
            startActivity(intent);

        }

    }
}
