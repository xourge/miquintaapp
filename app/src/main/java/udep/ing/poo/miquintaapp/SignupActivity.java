package udep.ing.poo.miquintaapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class SignupActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        botonRegistrar = (Button)findViewById(R.id.button);
        botonRegistrar.setOnClickListener(this);

        editUsuario = (EditText)findViewById(R.id.registrarUsuario);
        editContrasena = (EditText)findViewById(R.id.registrarContrasena);
        editContrasena2 = (EditText)findViewById(R.id.registrarContrasena2);
    }

    Button botonRegistrar;
    EditText editUsuario, editContrasena, editContrasena2;

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.button) {

            String inputUsuario = editUsuario.getText().toString();
            String inputContrasena = editContrasena.getText().toString();
            String inputContrasena2 = editContrasena2.getText().toString();

            if (inputUsuario.equals("")) { // usuario está en blanco
                Toast.makeText(this,getString(R.string.alertaUsuario),Toast.LENGTH_SHORT).show();

            } else if (inputContrasena.equals("") || inputContrasena2.equals("")) { // contraseña está en blanco
                Toast.makeText(this,getString(R.string.alertaContrasena),Toast.LENGTH_SHORT).show();

            } else if (!inputContrasena.equals(inputContrasena2)) { // contraseñas no son iguales
                Toast.makeText(this,getString(R.string.alertaContrasenasIguales),Toast.LENGTH_SHORT).show();

            } else {

                Usuarios usuarios = new Usuarios(this.getSharedPreferences("miquintaapp",Context.MODE_PRIVATE));

                usuarios.guardar(inputUsuario,inputContrasena);
                usuarios.escribirUsuarios();

                Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra("usuario",inputUsuario);
                startActivity(intent);

            }

        }

    }
}
