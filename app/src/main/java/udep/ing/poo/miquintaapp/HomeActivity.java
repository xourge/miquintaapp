package udep.ing.poo.miquintaapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        textView3 = (TextView)findViewById(R.id.textView3);

        Bundle bundle = getIntent().getExtras();
        String inputUsuario = (String)bundle.get("usuario");
        textView3.setText(inputUsuario);
    }

    TextView textView3;
}
